var path = require('path');
const npm_package = require('./package.json')
module.exports = {
  resolve: {
    alias: {
      'Components': path.resolve(__dirname, './src/app/components'),
      'Utils': path.resolve(__dirname, './src/app/utils'),
    },
  },
};