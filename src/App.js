import React, { Component } from 'react';
import './App.css';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { NavigationFrame } from './app/components';
import { Route, Switch } from 'react-router-dom';
import { withRouter } from "react-router-dom";
import { Page1 } from "./routes";
import { Page2 } from "./routes";
import { PageHome } from "./routes";
import { Page404 }  from './routes';

let pageMapping = {
  1: '/Home',
  2: '/Page1',
  3: '/Page2',
  101: '/Home/Subpage1',
}
class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: -1,
      breadCrumbs : '/'
    }
  }
  onSelectedSidebarIndexChanged(index) {
    // set page to an arbitrary path if the mapping does not exist -> displays the 404 page since its the default route
    let page = pageMapping[index]  === undefined ? '/pageMappingNonExistent_404' : pageMapping[index];
    this.props.history.push(page);
    // remove first / and replace all other / with ' > '
    let newBreadCrumbs = (""+this.props.history.location.pathname).substring(1).replace(/\//g , " > ");
    this.setState({
      selectedIndex: index,
      breadCrumbs: newBreadCrumbs
    });

    //TODO new bread crumbs component
    this.navFrame.setSidebarHeader(newBreadCrumbs);

  }
  render() {
    return (
      <div className="App">
        <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)}>
          <NavigationFrame title="App-Titel" ref={(navFrame) => this.navFrame = navFrame} breadCrumbs={this.state.breadCrumbs} selectedSidebarIndexChanged={this.onSelectedSidebarIndexChanged.bind(this)}>
            <Switch>
              <Route exact path='/Home' component={PageHome} />
              <Route exact path='/Page1' component={Page1} />
              <Route exact path='/Page2' component={Page2} />
              <Route component={Page404} />
            </Switch>
          </NavigationFrame>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default withRouter(App);