import React from 'react';
import Drawer from 'material-ui/Drawer';
import PropTypes from 'prop-types';
import { List, makeSelectable } from 'material-ui/List';

require("./Sidebar.css");

class Sidebar extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            open: true,
            top: 64
        }; //TODO

        if (props.top) {
            this.state.top = props.top;
        }

        if (props.selectedIndexChanged) {
            this.selectedIndexChanged = props.selectedIndexChanged;
        }
        else {
            this.selectedIndexChanged = () => { };
        }

    }

    componentWillMount() {
        this.setState({
            selectedIndex: this.props.defaultValue,
        });
    }
    handleToggle() {
        this.setState({ open: !this.state.open });
    };

    handleRequestChange(event, index) {
        this.setState({
            selectedIndex: index,
        });
        this.selectedIndexChanged(index);
    };

    render() {

        let SelectableList = makeSelectable(List);
        return (
            <div>
                <Drawer open={this.state.open} docked={true} containerStyle={{ height: `calc(100% - ${this.state.top}px)`, top: this.state.top }}>
                    <SelectableList className="sidebar-list" value={this.state.selectedIndex}
                        onChange={this.handleRequestChange.bind(this)}>
                        {this.props.children}
                    </SelectableList>
                </Drawer>
            </div>
        );
    }
}

Sidebar.propTypes = {
    children: PropTypes.node,
    defaultValue: PropTypes.number.isRequired,
    selectedIndex: PropTypes.number,
    top: PropTypes.number
};

export default Sidebar;