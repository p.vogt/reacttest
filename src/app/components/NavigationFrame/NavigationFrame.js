import React from 'react';
import AppBar from 'material-ui/AppBar';
import Subheader from 'material-ui/Subheader';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import { ListItem } from 'material-ui/List';
import PropTypes from 'prop-types';
import { default as Sidebar } from './Sidebar/Sidebar';

require("./NavigationFrame.css")

class NavigationFrame extends React.Component {

    constructor(props) {
        super(props);
        this.state = { open: true,
            contentClass: 'app-content expanded',
            selectedPage: -1,
            breadCrumbs: props.breadCrumbs,
            appbarHeight: 64};
        this.title = props.title;
        
        if(props.appbarHeight) {
            this.state.appbarHeight = props.appbarHeight;
        }
        if(props.selectedSidebarIndexChanged) {
            this.selectedSidebarIndexChanged = props.selectedSidebarIndexChanged;
        }
        else {
            this.selectedSidebarIndexChanged = () => {};
        }
        
    }

    handleToggle() {
        this.setState({ open: !this.state.open });
        this.sidebar.handleToggle();  ///calling a child function here
        if(this.state.open) {
            this.setState({contentClass: 'app-content'});
        }
        else {
            this.setState({contentClass: 'app-content expanded'});
        }

    };
    setSidebarHeader(value) {
        this.setState({
            breadCrumbs: value,
        });
    }

    onSidebarSelectionChanged(index) {
        this.setState({
            selectedPage: index,
        });
        this.selectedSidebarIndexChanged(index);
    }
    render() {

        // TODO -> Komponente
        const ButtonRight = (props) => (
            <IconMenu
              {...props}
              iconButtonElement={
                <IconButton><MoreVertIcon /></IconButton>
              }
              targetOrigin={{horizontal: 'right', vertical: 'top'}}
              anchorOrigin={{horizontal: 'right', vertical: 'top'}}
            >
              <MenuItem primaryText="Refresh" />
              <MenuItem primaryText="Help" />
              <MenuItem primaryText="..." />
            </IconMenu>
          );
          
          ButtonRight.muiName = 'IconMenu';

          return (
            <div className="navigation-frame">
                <AppBar className="app-bar"
                    title={this.title}
                    onLeftIconButtonClick={this.handleToggle.bind(this)}
                    iconElementRight={<ButtonRight/>}
                    style = {{height: this.state.appbarHeight}}
                    titleStyle = {{height: this.state.appbarHeight}}
                  />
                <Sidebar className="sidebar" top={this.state.appbarHeight} ref={(childRef) => this.sidebar = childRef} defaultValue={this.state.selectedPage} selectedIndexChanged={this.onSidebarSelectionChanged.bind(this)}>
                    <Subheader>{this.state.breadCrumbs}</Subheader>
                    <ListItem className="sidebar-list-item"
                        value={1}
                        primaryText="Home"
                        primaryTogglesNestedList= {true}
                        onClick= {(event,test) => {
                            console.log(test);
                        }}
                        nestedItems={[
                            <ListItem className="sidebar-list-item"
                                value={101}
                                primaryText="Subpage 1"
                            />,
                        ]}
                    />
                    <ListItem className="sidebar-list-item"
                        value={2}
                        primaryText="Page 1"
                        primaryTogglesNestedList= {true}
                        nestedItems={[
                            <ListItem className="sidebar-list-item"
                                value={201}
                                primaryText="Subpage 2"
                            />,
                        ]}
                    />
                    <ListItem className="sidebar-list-item"
                        value={3}
                        primaryText="Page 2"
                    />
                    <ListItem
                        value={4}
                        primaryText="404"
                    />
                </Sidebar>
                <div className={this.state.contentClass}>
                    {this.props.children}
                </div>
            </div>

        );
    }
}

NavigationFrame.propTypes = {
    breadCrumbs: PropTypes.string.isRequired,
    children: PropTypes.any,
    selectedSidebarIndexChanged: PropTypes.func,
    appbarHeight: PropTypes.number,
};

export default NavigationFrame;