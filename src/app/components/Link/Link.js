import React from 'react';
import PropTypes from 'prop-types'; 
const Link = (props) => {
  return (
    <a
      href={props.href}
    >
      {props.children}
    </a>
  );
};

Link.propTypes = {
  href: PropTypes.string
};

export default Link;