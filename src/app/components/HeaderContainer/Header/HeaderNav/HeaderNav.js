import React from 'react';
import { Link }  from '../../../../components'

import formatDate from '../../../../utils/formatDate';
import getAtPath from '../../../../utils/getAtPath';
import toNumber from '../../../../utils/toNumber';
import toString from '../../../../utils/toString';

const HeaderNav = () => {
  return (
    <nav>
      {formatDate(new Date())} <br />
      {getAtPath('something')} <br />
      {toNumber('1')} <br />
      {toString(1)}
      <br />
      <Link href="https://www.google.com">Nav 1</Link>
      <br />
      <Link href="https://www.google.com">Nav 2</Link>
      <br />
      <Link href="https://www.google.com">Nav 3</Link>
      <br />
    </nav>
  );
};

export default HeaderNav;