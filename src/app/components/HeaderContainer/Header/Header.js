import React from 'react';
import PropTypes from 'prop-types'; 
import HeaderNav from './HeaderNav/HeaderNav';

const Header = (props) => {
  return (
    <header>
      <h1>{props.title}</h1>

      <HeaderNav/>
    </header>
  );
};

Header.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Header;