import React from 'react';
import TextField from 'material-ui/TextField';
import { orange500, blue500 } from 'material-ui/styles/colors';

require("./Page1.css");
const styles = {
    errorStyle: {
        color: orange500,
    },
    underlineStyle: {
        borderColor: orange500,
    },
    floatingLabelStyle: {
        color: orange500,
    },
    floatingLabelFocusStyle: {
        color: blue500,
    },
};

const Page1 = () => (
    <div>
        <TextField
            floatingLabelText="Name (click me):"
            floatingLabelStyle={styles.floatingLabelStyle}
            floatingLabelFocusStyle={styles.floatingLabelFocusStyle}
        /><br />
        <TextField
            hintText="Styled Hint Text"
            hintStyle={styles.errorStyle}
        /><br />
        <TextField
            hintText="Custom error color"
            errorText="This field is required."
            errorStyle={styles.errorStyle}
        /><br />
        <TextField
            hintText="Custom Underline Color"
            underlineStyle={styles.underlineStyle}
        /><br />
        <TextField
            hintText="Custom Underline Focus Color"
            underlineFocusStyle={styles.underlineStyle}
        /><br />
    </div>
);

export default Page1;