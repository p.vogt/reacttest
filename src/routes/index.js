export {default as PageHome} from './PageHome/PageHome';
export {default as Page1} from './Page1/Page1';
export {default as Page2} from './Page2/Page2';
export {default as Page404} from './Page404/Page404';